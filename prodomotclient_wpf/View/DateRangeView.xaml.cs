﻿using prodomotclient_wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for DateRangeView.xaml
    /// </summary>
    public partial class DateRangeView : UserControl
    {
        public DateRangeView()
        {
            InitializeComponent();
        }

        private void Search_click(object sender, RoutedEventArgs e)
        {
             ((DateRangeViewModel)DataContext).ShowPomodoro((DateTime)DateStart.SelectedDate.Value, (DateTime)DateEnd.SelectedDate.Value);
        }
    }
}
