﻿using prodomotclient_wpf.infrastructure;
using prodomotclient_wpf.Infrastructure;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {

        Navigation nav = new Navigation();

        public LoginView()
        {
            InitializeComponent();
        }

        private void Btn_login(object sender, RoutedEventArgs e)
        {
            AuthService auth = new AuthService();
            User user = auth.Authenticate(LoginInput.Text, PasswordInput.Text);

            if (user == null)
            {
                MessageBox.Show("Email ou mot de passe incorrect !");
                nav.NavToApp(); // Temporaire !
                return;
            } else
            {
                GlobalUser.LoggedInUser = user;
                nav.NavToApp();
            }
        }

        private void NavigationToRegisterClick(object sender, RoutedEventArgs e)
        {
            nav.NavToRegister();
        }

    }
}
