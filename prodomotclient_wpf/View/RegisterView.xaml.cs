﻿using prodomotclient_wpf.infrastructure;
using prodomotclient_wpf.Infrastructure;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : UserControl
    {
        public RegisterView()
        {
            InitializeComponent();
        }

        Navigation nav = new Navigation();

        public void NavigationToLoginClick(object sender, RoutedEventArgs e)
        {
            nav.NavToLogin();
        }

        private void btn_registerUser(object sender, RoutedEventArgs e)
        {
            AuthService auth = new AuthService();

            User user = auth.RegisterUser(LoginInput.Text, EmailInput.Text, PasswordInput.Text);
            if (user == null)
            {
                MessageBox.Show("Une erreur est survenue");
                return;
            }

            GlobalUser.LoggedInUser = user;
            nav.NavToLogin();
        }

    }
}
