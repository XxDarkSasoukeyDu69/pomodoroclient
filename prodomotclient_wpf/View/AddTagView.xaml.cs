﻿using prodomotclient_wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for AddTagView.xaml
    /// </summary>
    public partial class AddTagView : UserControl
    {
        public AddTagView()
        {
            InitializeComponent();
            ((AddTagViewModel)DataContext).ShowTags();
        }
        
        //Fonction qui supprime un tag, il serait preferable de le declancher par le biais des ICommand
        private void DeleteTag_click(object sender, RoutedEventArgs e)
        {
            var valTag = ((Button)sender).Tag;
            var flightId = Convert.ToInt32(valTag);
            ((AddTagViewModel)DataContext).DeleteTag(flightId);
            ((AddTagViewModel)DataContext).ShowTags();
        }

        //Fonction qui ajoute un tag, il serait preferable de le declancher par le biais des ICommand
        private void AddTag_click(object sender, RoutedEventArgs e)
        {
            ((AddTagViewModel)DataContext).AddTag(NameTag.Text);
        }

    }
}
