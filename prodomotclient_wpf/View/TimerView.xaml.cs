﻿using prodomotclient_wpf.infrastructure;
using prodomotclient_wpf.Infrastructure;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using prodomotclient_wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for TimerView.xaml
    /// </summary>
    public partial class TimerView : UserControl
    {

        Navigation nav = new Navigation();

        private System.Windows.Forms.Timer timer;
        private int counter = 25;
        private int nbr_time = 1;

        public TimerView()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            timer = new System.Windows.Forms.Timer();
            timer.Tick += new EventHandler(timerTick);
            timer.Interval = 1000;
        }

         void setPlayTimerClick(object sender, EventArgs e)
         {
            ButtonStop.Visibility = Visibility.Visible;
            ButtonPlay.Visibility = Visibility.Collapsed;
            timer.Start();
        }

         void setStopTimerClick(object sender, EventArgs e)
         {
            ButtonStop.Visibility = Visibility.Collapsed;
            ButtonPlay.Visibility = Visibility.Visible;
            timer.Stop();
         }

        private void timerTick(object sender, EventArgs e)
        {
            counter--;
            if(counter == 0 || counter < 0)
            {
                nbr_time++;

                if (nbr_time%8 == 0)
                {
                    counter = 20;
                    ((TimerViewModel)DataContext).PutPomodoTask(); // Si le compteur passe a 20, je viens de finir un pomodoro donc je l'update

                    // fonction qui set le nouveau pomodoror courant
                } else if (nbr_time%2 == 0)
                {
                    counter = 5;
                    ((TimerViewModel)DataContext).PutPomodoTask(); // si le compteur passe a 5, je viens de finir un pomodoro donc je l'update

                    // fonction qui set le nouveau pomodoro courant
                } else
                {
                    counter = 25;
                }
                
            } else
            {
                timerCounter.Content = counter / 60 + ":" + ((counter % 60) >= 10 ? (counter % 60).ToString() : "0" + (counter % 60 ));
            }
        }

        private void NavigationToAddClick(object sender, RoutedEventArgs e)
        {
            nav.NavToAdd();
        }

        private void Button_UpClick(object sender, RoutedEventArgs e)
        {
            if (pomodoroList.SelectedIndex == -1)
            {
                MessageBox.Show("Select item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                int newIndex = pomodoroList.SelectedIndex - 1;
                if (newIndex < 0)
                    return;

                object selectItem = pomodoroList.SelectedItem;

                pomodoroList.Items.Remove(selectItem);

                pomodoroList.Items.Insert(newIndex, selectItem);

                pomodoroList.SelectedIndex = newIndex;

                ((TimerViewModel)DataContext).UpdatePomodoroTask(   /*...*/  ); // je renvoie ma liste modifier mais je ne sais pas actuellement si ca marche.

            }
        }

        private void Button_DownClick(object sender, EventArgs e)
        {
            if(pomodoroList.SelectedIndex == -1)
            {
                MessageBox.Show("Select item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                int newIndex = pomodoroList.SelectedIndex + 1;
                if(newIndex > pomodoroList.Items.Count)
                    return;

                object selectItem = pomodoroList.SelectedItem;

                pomodoroList.Items.Remove(selectItem);

                pomodoroList.Items.Insert(newIndex, selectItem);

                pomodoroList.SelectedIndex = newIndex;

                ((TimerViewModel)DataContext).UpdatePomodoroTask(  /*...*/  ); // je renvoie ma liste modifier, mais je ne sais pas actuellement si ca marche.
            }
        }

    }
}
