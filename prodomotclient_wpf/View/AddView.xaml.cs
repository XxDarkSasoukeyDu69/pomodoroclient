﻿using prodomotclient_wpf.Model;
using prodomotclient_wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for AddView.xaml
    /// </summary>
    public partial class AddView : UserControl
    {
        public AddView()
        {
            InitializeComponent();
        }

        // AUTOCOMPLETE TAG
        private void TagsInput_GotFocus(object sender, RoutedEventArgs e)
        {
            boxTagsSugg.Visibility = Visibility.Visible;
            ((AddViewModel)DataContext).UpdateTagsList(TagNameInput.Text);
        }
        private void TagsInput_LostFocus(object sender, RoutedEventArgs e)
        {
            boxTagsSugg.Visibility = Visibility.Hidden;
        }
        private void TagsInput_KeyUp(object sender, RoutedEventArgs e)
        {
            ((AddViewModel)DataContext).UpdateTagsList(TagNameInput.Text);
        }
        private void TagsInputSugg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TagsInputSugg.SelectedItem != null)
            {
                TagNameInput.Text = ((Tags)TagsInputSugg.SelectedItem).Name;
                TagIdHidden.Text = ((Tags)TagsInputSugg.SelectedItem).Id; // je stoke dans un input cacher mon id de tag selectionner pour ensuite l'inserer dans une liste d'id caché
                ((AddViewModel)DataContext).Select((Tags)TagsInputSugg.SelectedItem);
                TagsInputSugg.UnselectAll();
            }
        }

        private void Btn_addTag(object sender, RoutedEventArgs e)
        {
           if(TagNameInput.Text.Length > 0)
           {
               if ( !tagList.Items.Contains(TagNameInput.Text) ) { 
                    tagList.Items.Add(TagNameInput.Text);
                    tagListHidden.Items.Add(TagIdHidden.Text); // je me sert d'une liste cacher pour stoker mais id correspondant a mes tag.
               } 
           }
        }

        private void Btn_removeTag(object sender, RoutedEventArgs e)
        {
            object selectItem = tagList.SelectedItem;
            tagList.Items.Remove(selectItem);
        }

        private void Btn_add(object sender, RoutedEventArgs e)
        {
            // Problème, ma liste ne correspond pas a des tags id chose que je suis cen
            List<int> ints = new List<int>();
            foreach(int a in tagListHidden.Items)
            {
                ints.Add(a);
            }
            ((AddViewModel)DataContext).AddPomodoro(PomodoNameInput.Text, ints );
        }
    }
}
