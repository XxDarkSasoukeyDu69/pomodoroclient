﻿using prodomotclient_wpf.infrastructure;
using prodomotclient_wpf.Infrastructure;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prodomotclient_wpf.View
{
    /// <summary>
    /// Interaction logic for Navigation.xaml
    /// </summary>
    public partial class NavView : UserControl
    {

        Navigation nav = new Navigation();

        public NavView()
        {
            InitializeComponent();

            Navigation.Layout = this;

            isLoged();
        }

        private void isLoged()
        {
            AuthService auth = new AuthService();


            nav.NavToTimer();


            /* if (GlobalUser.LoggedInUser == null)
             {
                 MessageBox.Show("La session à du expiré veuillez vous reconnecter.");
                 nav.NavToLogin();
             }
             else
             {
                 User user = auth.GetUSerDetails(GlobalUser.LoggedInUser);
                 nav.NavToTimer();
             }   */
        }

        private void NavigationToAddClick(object sender, RoutedEventArgs e)
        {
            nav.NavToAdd();
        }

        private void NavigationToTimerClick(object sender, RoutedEventArgs e)
        {
            nav.NavToTimer();
        }

        private void NavigationToDateRangeClick(object sender, RoutedEventArgs e)
        {
            nav.NavToDateRange();
        }

        private void NavigationToAddTagClick(object sender, RoutedEventArgs e)
        {
            nav.NavToAddTag();
        }
    }
}
