﻿using prodomotclient_wpf.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.infrastructure
{
    class Navigation
    {
        public static MainView MainView { get; set; }

        public static NavView Layout { get; set; }

        #region Fonction de navigation

        public void NavToLogin()
        {
            MainView.ContentMainPage.Content = new LoginView();
        }

        public void NavToRegister()
        {
            MainView.ContentMainPage.Content = new RegisterView();
        }

        public void NavToApp()
        {
            MainView.ContentMainPage.Content = new NavView();
        }

        public void NavToTimer()
        {
            Layout.ContentLayout.Content = new TimerView();
        }

        public void NavToAdd()
        {
            Layout.ContentLayout.Content = new AddView();
        }

        public void NavToAddTag()
        {
            Layout.ContentLayout.Content = new AddTagView();
        }

        public void NavToDateRange()
        {
            Layout.ContentLayout.Content = new DateRangeView();
        }

        #endregion
    }
}
