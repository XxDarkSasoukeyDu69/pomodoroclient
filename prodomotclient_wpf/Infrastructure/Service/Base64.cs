﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Infrastructure.Service
{
    public class Base64
    {
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64Encode)
        {
            var base64EncodeBytes = System.Convert.FromBase64String(base64Encode);
            return System.Text.Encoding.UTF8.GetString(base64EncodeBytes);
        }

    }
}
