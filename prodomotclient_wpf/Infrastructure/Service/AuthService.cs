﻿using Newtonsoft.Json;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace prodomotclient_wpf.Infrastructure
{
    public class AuthService
    {

        /**
         * Base Url @string 
         */
        private string baseUrl;

        public AuthService()
        {
            this.baseUrl = "http://127.0.0.1/api/";
        }

        public User Authenticate(string login, string password)
        {
            string endpoint = this.baseUrl + "login";

            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authorization"] = "Basic " + Base64.Base64Encode(login + ":" + password);

            try
            {
                string response = wc.DownloadString(endpoint);
                return JsonConvert.DeserializeObject<User>(response);
            } catch(Exception)
            {
                return null;
            }
        }

        public User GetUSerDetails(User user)
        {
            string endpoint = this.baseUrl + "profil";
            string access_token = user.AccessToken;

            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authrization"] = "Bearer " + access_token;

            try
            {
                string response = wc.DownloadString(endpoint);
                user = JsonConvert.DeserializeObject<User>(response);
                user.AccessToken = access_token;
                return user;
            } catch(Exception)
            {
                return null;
            }
        }

        public User RegisterUser(string login, string email, string password)
        {
            string endpoint = this.baseUrl + "register";
            string methods = "POST";

            string json = JsonConvert.SerializeObject(new
            {
                email = email,
                fullname = login,
                password = password
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, methods, json);
                Console.WriteLine("TEST ::: " + response);
                return JsonConvert.DeserializeObject<User>(response);
            } catch (Exception)
            {
                return null;
            }
        }
       
    }
}
