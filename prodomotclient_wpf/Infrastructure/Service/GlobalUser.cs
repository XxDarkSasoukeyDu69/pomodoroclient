﻿using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Infrastructure.Service
{
    public class GlobalUser
    {
        public static User LoggedInUser { get; set; }
    }
}
