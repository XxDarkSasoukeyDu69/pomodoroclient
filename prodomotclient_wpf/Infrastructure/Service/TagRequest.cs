﻿using Newtonsoft.Json;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Infrastructure.Service
{
    public class TagRequest
    {
        private string baseUrl;

        public TagRequest()
        {
            this.baseUrl = "https://monurl.fr/api/";
        }

        public static void AddTag(string name)
        {
            dynamic obj = new TagRequest();
            string endpoint = obj.baseUrl + "tag";
            string methods = "POST";
            string access_token = GlobalUser.LoggedInUser.AccessToken;

            string json = JsonConvert.SerializeObject(new
            {
                name = name
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            wc.Headers["Autorization"] = "Bearer " + access_token; 

            try
            {
                string response = wc.UploadString(endpoint, methods, json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static Tags DeleteTag(int id)
        {
            dynamic obj = new TagRequest();
            string endpoint = obj.baseUrl + "tag";
            string methods = "DEL";
            string access_token = GlobalUser.LoggedInUser.AccessToken;

            string json = JsonConvert.SerializeObject(new
            {
                id = id
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            wc.Headers["Autorization"] = "Bearer " + access_token;
            try
            {
                string response = wc.UploadString(endpoint, methods, json);
                return JsonConvert.DeserializeObject<Tags>(response);
            } catch (Exception)
            {
                return null;
            }
        }

        public static List<Tags> getTags()
        {
            dynamic obj = new TagRequest();
            string endpoint = obj.baseUrl + "tag";
            string access_token = GlobalUser.LoggedInUser.AccessToken;

            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authrization"] = "Bearer " + access_token;

            try
            {
                string response = wc.DownloadString(endpoint);
                var list = JsonConvert.DeserializeObject<List<Tags>>(response);
                foreach (Tags a in list)
                {
                    list.Add(a);
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
