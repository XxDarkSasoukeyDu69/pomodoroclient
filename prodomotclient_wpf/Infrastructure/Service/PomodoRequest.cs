﻿using Newtonsoft.Json;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Infrastructure.Service
{
    public class PomodoRequest
    {

        private string baseUrl;

        public PomodoRequest()
        {
            this.baseUrl = "http://127.0.0.1.fr/api/";
        }

        public static List<Pomodoro> getPomodoros()
        {
            dynamic obj = new PomodoRequest();
            string endpoint = obj.baseUrl + "pomodoro";
            string access_token = GlobalUser.LoggedInUser.AccessToken;

            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authrization"] = "Bearer " + access_token;

            try
            {
                string response = wc.DownloadString(endpoint);
                var list = JsonConvert.DeserializeObject<List<Pomodoro>> (response);
                foreach (Pomodoro a in list) {
                    list.Add(a);
                }
                return list;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static List<Pomodoro> getPomodorosRangeDate(DateTime start, DateTime end)
        {
            dynamic obj = new PomodoRequest();
            string endpoint = obj.baseUrl + "pomodoro";
            string access_token = GlobalUser.LoggedInUser.AccessToken;
            string method = "GET";
            string json = JsonConvert.SerializeObject(new
            {
                start = start,
                end = end
            });

            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authrization"] = "Bearer " + access_token;

            try
            {
                string response = wc.UploadString(endpoint, method, json);
                var list = JsonConvert.DeserializeObject<List<Pomodoro>>(response);
                foreach (Pomodoro a in list)
                {
                    list.Add(a);
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void PutPomodo(Pomodoro pomodoro)
        {
            dynamic obj = new PomodoRequest();
            string endpoint = obj.baseUrl + "pomodoro";
            string method = "PUT";
            string access_token = GlobalUser.LoggedInUser.AccessToken;
            string json = JsonConvert.SerializeObject(new
            {
                id = pomodoro.Id,
                complete = pomodoro.Complete,
                elapsedTime = pomodoro.ElaspedTime,
                name = pomodoro.Name,
                completionDate = new DateTime()
            });
            
            WebClient wc = new WebClient();
            wc.Headers["content-type"] = "application/json";
            wc.Headers["Authrization"] = "Bearer " + access_token;

            try
            {
                string response = wc.UploadString(endpoint, method, json);
            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static Pomodoro AddPomodoro(string name, List<int> tagsInt)
        {
            dynamic obj =  new PomodoRequest();
            string endpoint = obj.baseUrl + "pomodoro";
            string methods = "POST";

            string json = JsonConvert.SerializeObject(new
            {
                name = name,
                completionDate = new DateTime(),
                tags = tagsInt
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, methods, json);
                return JsonConvert.DeserializeObject<Pomodoro>(response);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}