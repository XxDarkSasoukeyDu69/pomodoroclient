﻿using prodomotclient_wpf.Infrastructure;
using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace prodomotclient_wpf.ViewModels
{
    public class AddViewModel : BaseViewModel
    {


        #region Get/Set
        private string _name;
        private Tags _tags;
        private ObservableCollection<Tags> _tagSugg = new ObservableCollection<Tags>();
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public Tags Tags
        {
            get { return _tags; }
            set { _tags = value;  }
        }
        public ObservableCollection<Tags> TagsCollectionSugg
        {
            get { return _tagSugg; }
            set { _tagSugg = value; }
        }
        #endregion

        public void UpdateTagsList(string input)
        {
            List<Tags> tagsList;

            tagsList = TagRequest.getTags();

            _tagSugg.Clear();
            if (input != "")
            {
                foreach (Tags a in tagsList)
                {
                    _tagSugg.Add(a);
                }
            }
        }

        public void SetName(string pomodoro)
        {
            Name = pomodoro;
        }

        public void Select(Tags tags)
        {
            Tags = tags;
        }

        public void AddPomodoro(string name, List<int> intList)
        {
            PomodoRequest.AddPomodoro(name, intList);
        }
    }
}
