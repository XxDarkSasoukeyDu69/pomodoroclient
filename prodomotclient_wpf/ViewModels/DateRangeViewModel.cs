﻿using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.ViewModels
{
    public class DateRangeViewModel
    {
        private ObservableCollection<Pomodoro> _pomodoros = new ObservableCollection<Pomodoro>();

        public ObservableCollection<Pomodoro> PomodoroSugg
        {
            get { return _pomodoros; }
            set { _pomodoros = value; }
        }

        public void ShowPomodoro(DateTime start, DateTime end)
        {
            List<Pomodoro> pomodoroList;
            pomodoroList = PomodoRequest.getPomodorosRangeDate(start, end);

            _pomodoros.Clear();

            foreach (Pomodoro a in pomodoroList)
            {
                _pomodoros.Add(a);
            }

        }

    }
}
