﻿using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace prodomotclient_wpf.ViewModels
{
    public class TimerViewModel : BaseViewModel
    {
        private ObservableCollection<Pomodoro> _pomodoro = new ObservableCollection<Pomodoro>();

        public ObservableCollection<Pomodoro> Pomodoro
        {
            get { return _pomodoro; }
            set { _pomodoro = value; }
        }

        public void UpdatePomodoroTask(List<Pomodoro> pomodoroList)
        {
            _pomodoro.Clear();
            foreach (Pomodoro p in pomodoroList)
            {
                _pomodoro.Add(p);
            }
        }

        public void PutPomodoTask(Pomodoro pomodoro)
        {
            PomodoRequest.PutPomodo(pomodoro);
        }

        public void GetPomodoTask()
        {
            List<Pomodoro> pomodoroList;
            pomodoroList = PomodoRequest.getPomodoros();
            _pomodoro.Clear();
            foreach(Pomodoro p in pomodoroList)
            {
                _pomodoro.Add(p);
            }
        }
    }
}
