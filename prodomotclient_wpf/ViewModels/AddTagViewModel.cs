﻿using prodomotclient_wpf.Infrastructure.Service;
using prodomotclient_wpf.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.ViewModels
{
    public class AddTagViewModel : BaseViewModel
    {

        private ObservableCollection<Tags> _tagSugg = new ObservableCollection<Tags>();

        public ObservableCollection<Tags> TagSugg
        {
            get { return _tagSugg; }
            set { _tagSugg = value; }
        }

        public void ShowTags()
        {
            List<Tags> tagsList;
            tagsList = TagRequest.getTags();

            _tagSugg.Clear();

            foreach(Tags a in tagsList)
            {
                _tagSugg.Add(a);
            }
        }

        public void AddTag(string name)
        {   
            TagRequest.AddTag(name);
            ShowTags(); // Logiquement ca update mon observableCollection mais ce n'est pas tres bien de le faire ainsi
        }

        public void DeleteTag(int id)
        {
            TagRequest.DeleteTag(id);
            ShowTags(); // Logiquement ca update mon observableCollection mais ce n'est pas très bien de le faire ainsi
        }
    }
}
