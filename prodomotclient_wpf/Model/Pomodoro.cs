﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Model
{
    public class Pomodoro
    {
        private string _id;
        private int _elaspedTime;
        private bool _complete;
        private string _userId;
        private string _name;
       // private test = new List<Tags>();

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int ElaspedTime
        {
            get { return _elaspedTime; }
            set { _elaspedTime = value; }
        }
        public bool Complete
        {
            get { return _complete; }
            set { _complete = value; }
        }
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
