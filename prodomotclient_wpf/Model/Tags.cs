﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Model
{
    public class Tags
    {
        private string _id;
        private string _userId;
        private string _name;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
