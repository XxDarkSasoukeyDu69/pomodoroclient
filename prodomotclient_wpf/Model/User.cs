﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prodomotclient_wpf.Model
{
    public class User
    {
        private string _id;
        private string _login;
        private string _email;
        private string _password;
        private string _access_token;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Loggin
        {
            get { return _login; }
            set { _login = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string AccessToken
        {
            get { return _access_token; }
            set { _access_token = value; }
        }
    }
}
